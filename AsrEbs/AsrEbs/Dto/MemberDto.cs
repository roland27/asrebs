﻿using AsrEbs.ApiModel;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.Dto
{
    public class MemberDto
    {
        [Key]
        public int? MemberId { get; set; }

        public string Name { get; set; }

        public ICollection<TripMemberRelationDto> TripMemberRelations { get; set; }

        public Member toApiModel()
        {
            var memberModel = new Member();
            memberModel.MemberId = MemberId;
            memberModel.Name = Name;
            return memberModel;
        }
    }
}
