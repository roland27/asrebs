﻿using AsrEbs.ApiModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.Dto
{
    public class BoatDto
    {
        [Key]
        public int? BoatId { get; set; }
        public string BoatName { get; set; }
        public string BoatType { get; set; }
        public int MaxMembersInBoat { get; set; }

        internal Boat toApiModel()
        {
            var boatModel = new Boat();
            boatModel.BoatId = BoatId;
            boatModel.BoatName = BoatName;
            boatModel.BoatType = BoatType;
            boatModel.MaxMembersInBoat = MaxMembersInBoat;
            return boatModel;
        }
    }
}
