﻿using AsrEbs.ApiModel;
using AsrEbs.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.Dto
{
    public class TripDto
    {
        [Key]
        public int? TripId { get; set; }
        public string TripDestination { get; set; }
        public DateTime? TripStartTime { get; set; }

        public int BoatId { get; set; }

        public ICollection<TripMemberRelationDto> TripMemberRelations { get; set; }

        public Trip toApiModel()
        {
            var model = new Trip();
            model.TripId = TripId;
            model.TripDestination = TripDestination;
            model.TripStartTime = TripStartTime;
            model.Boat = null;
            return model;
        }

    }
}
