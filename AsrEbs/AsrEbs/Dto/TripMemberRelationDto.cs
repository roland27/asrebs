﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.Dto
{
    public class TripMemberRelationDto
    {
        public int? TripId { get; set; }
        public TripDto Trip { get; set; }
        public int? MemberId { get; set; }
        public MemberDto Member { get; set; }
    }
}
