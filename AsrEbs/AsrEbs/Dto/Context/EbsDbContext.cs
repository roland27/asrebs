﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.Dto.Context
{
    public class EbsDbContext : DbContext
    {
        public EbsDbContext(DbContextOptions<EbsDbContext> options) : base(options)
        { }

        public DbSet<TripDto> Trips { get; set; }
        public DbSet<MemberDto> Members { get; set; }
        public DbSet<BoatDto> Boats { get; set; }
        public DbSet<TripMemberRelationDto> TripMemberRelations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure many-to-many relation between Trip and Member using relation table
            modelBuilder.Entity<TripMemberRelationDto>(eb =>
            {
                eb.HasKey(tmr => new { tmr.TripId, tmr.MemberId });
                eb.HasOne(tmr => tmr.Trip)
                    .WithMany(t => t.TripMemberRelations)
                    .HasForeignKey(tmr => tmr.TripId);
                eb.HasOne(tmr => tmr.Member)
                    .WithMany(t => t.TripMemberRelations)
                    .HasForeignKey(tmr => tmr.MemberId);

            });

            modelBuilder.Entity<TripDto>(t =>
            {
                t.Property(e => e.TripId).UseIdentityColumn();
            });

            modelBuilder.Entity<MemberDto>(t =>
            {
                t.Property(e => e.MemberId).UseIdentityColumn();
            });

            modelBuilder.Entity<BoatDto>(t =>
            {
                t.Property(e => e.BoatId).UseIdentityColumn();
            });
        }

    }
}
