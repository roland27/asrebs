﻿using AsrEbs.Dto;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.ApiModel
{
    public class Member
    {
        public int? MemberId { get; set; }
        public string Name { get; set; }

        public MemberDto toMemberDto()
        {
            var memberDto = new MemberDto();
            memberDto.MemberId = MemberId;
            memberDto.Name = Name;
            return memberDto;
        }
    }
}
