﻿using AsrEbs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.ApiModel
{
    public class TripWithMembers
    {
        //todo - stop using dto here
        public Trip Trip { get; set; }
        public List<Member> Members { get; set; }
    }
}
