﻿using AsrEbs.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.ApiModel
{
    public class Trip
    {
        public int? TripId { get; set; }
        public string TripDestination { get; set; }
        public DateTime? TripStartTime { get; set; }

        public Boat Boat { get; set; }

        public TripDto ToTripDto()
        {
            var dto = new TripDto();
            dto.TripDestination = TripDestination;
            dto.TripStartTime = TripStartTime;
            dto.BoatId = Boat.BoatId.GetValueOrDefault();

            return dto;
        }

    }
}
