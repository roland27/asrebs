﻿using AsrEbs.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AsrEbs.ApiModel
{
    public class Boat
    {
        public int? BoatId { get; set; }
        public string BoatName { get; set; }
        public string BoatType { get; set; }
        public int MaxMembersInBoat { get; set; }

        public BoatDto toBoatDto()
        {
            var dto = new BoatDto();
            dto.BoatId = BoatId;
            dto.BoatName = BoatName;
            dto.BoatType = BoatType;
            dto.MaxMembersInBoat = MaxMembersInBoat;
            return dto;
        }
    }
}
