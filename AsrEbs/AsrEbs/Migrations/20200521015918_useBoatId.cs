﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AsrEbs.Migrations
{
    public partial class useBoatId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Boats_BoatId",
                table: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Trips_BoatId",
                table: "Trips");

            migrationBuilder.AlterColumn<int>(
                name: "BoatId",
                table: "Trips",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BoatId",
                table: "Trips",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Trips_BoatId",
                table: "Trips",
                column: "BoatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Boats_BoatId",
                table: "Trips",
                column: "BoatId",
                principalTable: "Boats",
                principalColumn: "BoatId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
